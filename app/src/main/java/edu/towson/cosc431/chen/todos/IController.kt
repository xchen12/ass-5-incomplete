package edu.towson.cosc431.chen.todos

interface IController {
    // TODO - update interface to handle recyclerview
    fun toggleComplete(idx:Int)
    fun addTodo(todo: Todo)
    fun removeTodo(idx:Int)

}