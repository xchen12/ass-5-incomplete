package edu.towson.cosc431.chen.todos

import android.app.Activity
import android.content.Intent
import android.graphics.Paint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.SpannableString
import android.text.style.StrikethroughSpan
import android.util.Log
import android.widget.TextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.todo_view.*
import java.util.*

class MainActivity : AppCompatActivity(),IController {

    override fun addTodo(todo: Todo) {
        TodoList.add(todo)


    }

    override fun removeTodo(idx: Int) {
         TodoList.removeAt(idx)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {launchNewTodoActivity()}

        val todoAdapter = todoAdapter(TodoList,this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = todoAdapter

        val itemHelper = TodoItemHelper(TodoCallback(recyclerView.adapter as todoAdapter))
        itemHelper.attachToRecyclerView(recyclerView)
        populateTodoList()
    }
    private fun launchNewTodoActivity() {
        val intent = Intent(this,NewTodoActivity::class.java)
        startActivityForResult(intent, To_Do_REQUEST_CODE)
    }

    override fun toggleComplete(idx:Int) {
        val todo = TodoList.get(idx)
        TodoList.set(idx, todo.copy(isComplete = !todo.isComplete))

    }
    var TodoList: MutableList<Todo> = mutableListOf()
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            To_Do_REQUEST_CODE -> {
                when(resultCode){
                    Activity.RESULT_OK ->
                    {
                        val json = data?.extras?.get(NewTodoActivity.TODO).toString()
                        val todo = Gson().fromJson<Todo>(json, Todo::class.java)
                        TodoList.add(todo)
                        recyclerView.adapter?.notifyDataSetChanged()
                        Log.d("MainActivity", "Result was OK")
                    }
                    Activity.RESULT_CANCELED -> {
                        Log.d("MainActivity", "Result was CANCELLED")
                    }
                }

            }
        }
    }
    companion object {
        val To_Do_REQUEST_CODE = 1
    }

    private fun populateTodoList() {
      (1..20).forEach {
          TodoList.add(Todo("Title"+it, "Content"+it,  it%5==0 , Date().toString()))
       }

      //  TodoList = (1..100).map { Todo("Title"+it, "Content"+it,  it%3==0,  date = Date().toString()) }.toMutableList()
    }
}
