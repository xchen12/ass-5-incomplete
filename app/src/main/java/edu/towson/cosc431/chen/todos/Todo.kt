package edu.towson.cosc431.chen.todos

import java.util.*
import com.google.gson.Gson
import java.text.DateFormat

data class Todo (
        val title :String,
        val content : String,
        val isComplete : Boolean,
        val date: String){

        fun toJson(): String{
            return Gson().toJson(this)
    }
}

