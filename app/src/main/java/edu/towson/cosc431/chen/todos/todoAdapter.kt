package edu.towson.cosc431.chen.todos

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.todo_view.view.*
import java.util.*
import kotlin.coroutines.experimental.coroutineContext


class todoAdapter(val todoList:MutableList<Todo>,val controller:IController):RecyclerView.Adapter<TodoViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder{

        val view = LayoutInflater.from(parent.context).inflate(R.layout.todo_view,parent,false)

        val viewHolder  = TodoViewHolder(view)

        viewHolder.itemView.isCompleteCb.setOnClickListener {
            val position = viewHolder.adapterPosition
            controller.toggleComplete(position)
            notifyItemChanged(position)
        }

      /*  viewHolder.itemView.setOnLongClickListener {

            val position = viewHolder.adapterPosition
            controller.removeTodo(position)
           notifyItemRemoved(position)

            true
        }
*/

        viewHolder.itemView.setOnClickListener {
            val intent = Intent(view.context,NewTodoActivity::class.java)
            //intent.putExtra(TODO(),todoList.toString())
            view.context.startActivity(intent)
        }

       return viewHolder

    }

    override fun getItemCount(): Int {
        return todoList.size
    }

    override fun onBindViewHolder(holder:TodoViewHolder, position: Int) {

        val todo =todoList.get(position)

        holder.itemView.Todo_Title.text = todo.title
        holder.itemView.Todo_Contents.text = todo.content
        holder.itemView.isCompleteCb.isChecked = todo.isComplete
        holder.itemView.Todo_Date.text = todo.date


    }


    fun onItemDelete(position: Int?) {
        todoList?.removeAt(position!!)
        notifyItemRemoved(position!!)
    }

    fun onItemMove(from: Int, to: Int): Boolean {
        if (from < to) {
            for (i in from until to) {
                Collections.swap(todoList, i, i + 1)
            }
        } else {
            for (i in from downTo to + 1) {
                Collections.swap(todoList, i, i - 1)
            }
        }
        notifyItemMoved(from, to)
        return true
    }

}


class TodoViewHolder (view : View?) : RecyclerView.ViewHolder(view){


}
