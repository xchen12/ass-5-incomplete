package edu.towson.cosc431.chen.todos

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_new_todo.*
import com.google.gson.Gson
import java.text.DateFormat
import java.text.DateFormat.MEDIUM
import java.text.DateFormat.getDateInstance
import java.util.*

class NewTodoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)

        button3.setOnClickListener { returnInformation() }
    }
    private fun returnInformation() {

        val dataCreated = Date().toString()

        val todo = Todo(
                editText.editableText.toString(),
                comment2.editableText.toString(),
                checkBox.isChecked,
                dataCreated)
        val bundle = Intent()
        bundle.putExtra(TODO, todo.toJson())
        setResult(RESULT_OK, bundle)
        // kill this activity and resume MainActivity
        finish()
    }

    companion object {
        val TODO = "Todo"
    }
}
